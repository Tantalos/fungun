package de.tantalos.fungun;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import de.tantalos.eventlistener.EventCallback;
import de.tantalos.eventlistener.PlayerInteractEventListener;
import de.tantalos.eventlistener.ProjectileHitEventListener;

public class FunGunBehavior {

    // fun gun properties
    private final Plugin plugin;
    private final Class<? extends Projectile> projectileClass;
    private final ImmutableSet<Sound> sounds;
    private final float soundPitch;
    private final float soundVolume;
    private final ImmutableMap<Effect, Integer> effects;
    private final int projectileAmount;
    private final int waitingTime;

    final Set<ItemStack> registeredFunguns = new HashSet<ItemStack>();
    private final Set<Projectile> effectProjectiles = new HashSet<Projectile>();
    private final Set<Projectile> soundProjectiles = new HashSet<Projectile>();

    private final Map<Player, Date> playerFiredBulletInTime = new HashMap<>();

    private final String INVISIBLE_IDENTIFIER;

    public FunGunBehavior(Plugin plugin, Set<Sound> sounds, float soundPitch, float soundVolume,
            Map<Effect, Integer> effects, int projectileAmount,
            int waitingTime, Class<? extends Projectile> projectileClass) {
        this.plugin = plugin;
        this.sounds = ImmutableSet.<Sound>copyOf(sounds);
        this.soundPitch = soundPitch;
        this.soundVolume = soundVolume;
        this.effects = ImmutableMap.<Effect, Integer>copyOf(effects);
        this.projectileAmount = projectileAmount;
        this.waitingTime = waitingTime;
        this.projectileClass = projectileClass;

        // create random identifier
        Random random = new Random();
        String identifier = new BigInteger(130, random).toString(32);
        INVISIBLE_IDENTIFIER = createInvisibleString(identifier);

        EventCallback<PlayerInteractEvent> rifleTrigger = new PlayerTrigger();
        new PlayerInteractEventListener(this.plugin, rifleTrigger);

        EventCallback<ProjectileHitEvent> impactTrigger = new ProjectileHit();
        new ProjectileHitEventListener(this.plugin, impactTrigger);
    }

    public void createFunGun(ItemStack item) {
        Preconditions.checkNotNull(item);

        if (registeredFunguns.contains(item)) {
            return;
        }

        ItemStack fungun = makeItemStackIdentifiable(item);
        registeredFunguns.add(fungun);
    }

    /**
     * use item utils of itemset instead
     */
    @Deprecated
    private static String createInvisibleString(String string) {
        StringBuilder b = new StringBuilder();
        for (Character c : string.toCharArray()) {
            b.append(String.valueOf(ChatColor.COLOR_CHAR));
            b.append(c.toString());
        }
        return b.toString();
    }

    /**
     * use item utils of itemset instead
     */
    @Deprecated
    private ItemStack makeItemStackIdentifiable(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();

        if (lore == null) {
            lore = Lists.newArrayList();
        }

        /*
         * add invisible identifier string. This string will be checked in
         * equals/isSimilar methods
         */
        lore.add(INVISIBLE_IDENTIFIER);
        meta.setLore(lore);
        item.setItemMeta(meta);

        return item;
    }


    public class PlayerTrigger implements EventCallback<PlayerInteractEvent> {
        @Override
        public void onEvent(PlayerInteractEvent event) {
            Player player = event.getPlayer();
            final ItemStack item = event.getItem();
            if (item == null) {
                return;
            }

            // if player has permission
            if (!player.hasPermission("fungun.use")) {
                return;
            }

            // if item is a fungun
            if (!registeredFunguns.stream().anyMatch(x -> item.isSimilar(x))) {
                return;
            }

            if (!isShootDelayElapsed(player)) {
                return;
            }

            shootProjectiles(player);
        }
    }

    public class ProjectileHit implements EventCallback<ProjectileHitEvent> {
        @Override
        public void onEvent(ProjectileHitEvent event) {
            Projectile projectile = event.getEntity();
            // play sound
            if (soundProjectiles.remove(projectile)) {
                Location location = event.getEntity().getLocation();
                playSound(location);
            }

            // play effect
            if (effectProjectiles.remove(projectile)) {
                Location location = event.getEntity().getLocation();
                playEffect(location);
            }
        }
    }

    private boolean isShootDelayElapsed(Player player) {
        if (!playerFiredBulletInTime.containsKey(player)) {
            playerFiredBulletInTime.put(player, new Date());
            return true;
        }
        Date lastFireTime = playerFiredBulletInTime.get(player);
        if (lastFireTime.getTime() + waitingTime < new Date().getTime()) {
            playerFiredBulletInTime.put(player, new Date());
            return true;
        }
        return false;
    }

    private void shootProjectiles(Player player) {
        for (int i = 0; i < projectileAmount; i++) {
            Projectile projectile = player.launchProjectile(projectileClass);
            effectProjectiles.add(projectile);

            // send only one projectile with sound
            if (i == 0) {
                soundProjectiles.add(projectile);
            }
        }
    }

    private void playEffect(Location location) {
        World world = location.getWorld();

        for (Entry<Effect, Integer> effect : effects.entrySet()) {
            for (int i = 0; i < effect.getValue(); i++) {
                world.playEffect(location, effect.getKey(), 0);
            }
        }
	}

    private void playSound(Location location) {
        World world = location.getWorld();

        for (Sound sound : sounds) {
            world.playSound(location, sound, soundVolume, soundPitch);
        }
    }
}
