package de.tantalos.fungun;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public class FunGunExecutor implements CommandExecutor {

    private final Plugin plugin = FunGun.plugin;

    private final FunGunBehaviorBuilder fungunBehaviorBuilder;
    private FunGunBehavior fungunBehavior;
    private final Material defaultItem = Material.BLAZE_ROD;

    public FunGunExecutor() {
        fungunBehaviorBuilder = new FunGunBehaviorBuilder();
        fungunBehaviorBuilder.addEffect(Effect.LAVA_POP, 50);
        fungunBehaviorBuilder.addEffect(Effect.HEART, 3);
        fungunBehaviorBuilder.addSound(Sound.ENTITY_CAT_AMBIENT);

        this.fungunBehavior = fungunBehaviorBuilder.build(); // default behavior
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        if (args.length == 0) {
            return false;
        }
        switch (args[0]) {
        case "get":
            return get(sender, label, args);
        case "apply":
            return apply(sender, label, args);
        case "set":
            // TODO set behavior
        default:
            return false;
        }

    }


    /**
     * handles get command with signature /fungun get [player] [material] [lore]
     *
     * @param sender
     *            the command sender
     * @param label
     *            command label
     * @param args
     *            optional command arguments
     * @return true if command arguments are properly, false if not
     */
    private boolean get(CommandSender sender, String label, String[] args) {
        if (!sender.hasPermission("fungun.command.get")) {
            return true;
        }

        Player player;
        if (args.length <= 1) {
            player = (Player) sender;
        } else {
            player = plugin.getServer().getPlayer(args[1]);
            if (player == null) {
                player = (Player) sender;
            }
        }

        Material material = null;
        if (args.length <= 2) {
            material = defaultItem;
        } else {
            material = Material.valueOf(args[2].toUpperCase());
            if (material == null) {
                material = defaultItem;
            }
        }
        ItemStack item = new ItemStack(material);

        // simple lore hack
        if (args.length >= 3) {
            String lore = "";
            for (int i = 2; i < args.length; i++) {
                lore += args[i];
            }
            setLore(item, lore);
        }

        fungunBehavior.createFunGun(item);
        give(player, item);
        return true;
    }

    /**
     * handles the apply command with signature /apply [lore]
     *
     * @param sender
     *            command sender
     * @param label
     *            command label
     * @param args
     *            optional command arguments
     * @return true if command arguments are properly, false if not
     */
    private boolean apply(CommandSender sender, String label, String[] args) {
        if (!sender.hasPermission("fungun.command.apply")) {
            return true;
        }

        Player player = (Player) sender;
        ItemStack item = player.getEquipment().getItemInMainHand();

        // simple lore hack
        if (args.length >= 1) {
            String lore = "";
            for (int i = 1; i < args.length; i++) {
                lore += args[i];
            }
            setLore(item, lore);
        }
        fungunBehavior.createFunGun(item);

        return true;
    }

    private void setLore(ItemStack item, String text) {
        List<String> lore = new ArrayList<>();
        lore.add(" " + text);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(lore);
        item.setItemMeta(meta);

    }

    private void give(Player targetPlayer, ItemStack fungun) {
        targetPlayer.getInventory().addItem(fungun);
    }

}
