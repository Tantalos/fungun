package de.tantalos.fungun;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class FunGun extends JavaPlugin {

    public static Plugin plugin;

	@Override
    public void onEnable() {
        super.onEnable();
        plugin = this;

        FunGunExecutor fungunCommandHandler = new FunGunExecutor();
        getCommand("fungun").setExecutor(fungunCommandHandler);
	}

	@Override
    public void onDisable() {
        super.onDisable();
	}
}
