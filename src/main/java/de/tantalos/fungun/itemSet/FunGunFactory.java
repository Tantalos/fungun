package de.tantalos.fungun.itemSet;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;

import de.tantalos.fungun.FunGunBehavior;
import de.tantalos.fungun.FunGunBehaviorBuilder;
import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemBuilder;
import de.tantalos.itemset.connector.ItemFactory;
import de.tantalos.itemset.utils.StaticItem;

public class FunGunFactory implements ItemFactory<ItemBuilder> {

    /**
     * args are optional
     */
    @Override
    public ItemBuilder buildItemDefinition(String... args) throws IllegalConfigurationException {
        FunGunBehaviorBuilder builder = new FunGunBehaviorBuilder();
        Material material = Material.BLAZE_ROD; // default
        String line1 = null;
        String line2 = null;
        int amount = 1;

        if (args != null) {
            for (String arg : args) {
                String key = ItemFactory.getKey(arg);
                String value = ItemFactory.getValue(arg);
                try {
                    switch (key) {
                    case "effect":
                        Effect effect = Effect.valueOf(value);
                        builder.addEffect(effect, 20);
                        break;
                    case "sound":
                        Sound sound = Sound.valueOf(value);
                        builder.addSound(sound);
                        break;
                    case "material":
                        material = Material.valueOf(value);
                        break;
                    case "line1":
                    case "name":
                        line1 = value;
                        break;
                    case "line2":
                    case "lore":
                        line2 = value;
                        break;
                    case "amount":
                        amount = Integer.valueOf(value);
                        break;
                    case "projectile":
                        @SuppressWarnings("unchecked")
                        Class<? extends Projectile> projectile = (Class<? extends Projectile>) Class.forName(value);
                        builder.setProjectile(projectile);
                        break;
                    case "projectile-amount":
                        int projectileAmount = Integer.valueOf(value);
                        builder.setProjectileAmount(projectileAmount);
                        break;
                    default:
                        throw new IllegalConfigurationException("Unknown parameter key: " + key);
                    }
                } catch (IllegalArgumentException | ClassNotFoundException e) {
                    throw new IllegalConfigurationException("The value for key " + key + " can not be resolved", e);
                }
            }
        }

        final FunGunBehavior fungunGenerator = builder.build();
        ItemStack item = new StaticItem(material, amount, line1, line2).getItem();

        return new ItemBuilder() {
            @Override
            public ItemStack getItem() {
                fungunGenerator.createFunGun(item);
                return item;
            }
        };
    }

}
