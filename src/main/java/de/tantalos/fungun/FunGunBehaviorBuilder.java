package de.tantalos.fungun;

import java.util.Map;
import java.util.Set;

import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class FunGunBehaviorBuilder {

    private int projectileAmount = 5;
    private int waitingTime = 1000 * 2;
    private Class<? extends Projectile> projectileClass = Snowball.class;
    private Set<Sound> sounds = Sets.newHashSet();
    private float soundPitch = 2.0f;
    private float soundVolume = 5.0f;
    private Map<Effect, Integer> effects = Maps.newHashMap();

    public FunGunBehaviorBuilder() {
    }

    public FunGunBehaviorBuilder addSound(Sound sound) {
        Preconditions.checkNotNull(sound);
        sounds.add(sound);
        return this;
    }

    public FunGunBehaviorBuilder setSoundVolume(float volume) {
        Preconditions.checkArgument(volume > 0, "amount must be a positive number");
        soundVolume = volume;
        return this;
    }

    public FunGunBehaviorBuilder setSoundPitch(float pitch) {
        soundPitch = pitch;
        return this;
    }

    public FunGunBehaviorBuilder setSound(Set<Sound> sounds, int pitch, int volume) {
        Preconditions.checkNotNull(sounds);
        this.sounds = sounds;
        return this;
    }

    public FunGunBehaviorBuilder addEffect(Effect effect, int strenght) {
        Preconditions.checkNotNull(effect);
        Preconditions.checkArgument(strenght > 0 , "strength must be a positive number");
        effects.put(effect, strenght);
        return this;
    }

    public FunGunBehaviorBuilder setEffect(Map<Effect, Integer> effects) {
        Preconditions.checkNotNull(effects);
        this.effects = effects;
        return this;
    }

    public FunGunBehaviorBuilder setProjectileAmount(int projectileAmount) {
        this.projectileAmount = projectileAmount;
        return this;
    }

    public FunGunBehaviorBuilder setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
        return this;
    }

    public FunGunBehaviorBuilder setProjectile(Class<? extends Projectile> projectileClass) {
        this.projectileClass = projectileClass;
        return this;
    }

    public FunGunBehavior build() {
        return new FunGunBehavior(FunGun.plugin, sounds, soundPitch, soundVolume, effects, projectileAmount,
                waitingTime, projectileClass);

    }
}
