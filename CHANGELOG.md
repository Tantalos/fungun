## [0.0.6-SNAPSHOT] - 2017-03-30
### Fixed
- fixed bug: where the behavior and projectiles of several funguns was merged
- fixed bug: fungun wasn't triggered when player holds 2 funguns in a stack
 
### Added
- more configuration options and alias (name, lore, projectiles, projectile-amount)
- configuration option for projectiles (arrows, snow balls, ender pearls etc.)

## [0.0.5-SNAPSHOT] - 2017-03-12
### Added
- default values for fungun factory

## [0.0.4-SNAPSHOT] - 2017-03-10
### Changed
- repository for dependencies

## [0.0.3-SNAPSHOT] - 2017-03-06
### Added
- added support for item set