#FunGun

An item which shoot projectiles and sprays particles.

#Configuration

Use [**ItemSet**](https://bitbucket.org/Tantalos/itemset/wiki/Home) to configure FunGuns. You can use the following options as arguments



| configuration option | description |
|----------------------|---------------|
|effect | Defines the particles which show up, when fungun is used. You can use more than one effect. Use the enum value from [spigot effect](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Effect.html) (e.g. effect=LAVA_POP) |
|sound | Defines the sound which is played, when fungun is used. You can use more than one sound. Use the enum value from [spigot sound](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html) (e.g. sound=ENTITY_CAT_AMBIENT) |
|material | Defines the item which is the fungun. Use the enum value from [spigot material](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html) (e.g. material=BLAZE_ROD)|
|text | Defines the name of the item |
|amount | Defines the item count |
|projectile | Defines the projectiles which are shooten from the fungun. Use the projectile subclass [spigot projectiles](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Projectile.html) (e.g. projectile=org.bukkit.entity.Snowball) |
|projectile-amount | Define the amount of projectiles shooten from the fungun |


#[ItemSet](https://bitbucket.org/Tantalos/itemset/wiki/Home) Example
This is a example configuration for funguns. Define the FunGun in `<item-definition>` and use arguments to configure it. Use the `/itemset give <item id> [player name]` command to give funguns.

```
#!XML

<itemset>
    <item-definition>
        <item id="minimal-fungun" name="FunGun" item-factory="de.tantalos.fungun.itemSet.FunGunFactory" />
        <item id="fungun" name="FunGun" item-factory="de.tantalos.fungun.itemSet.FunGunFactory">
            <arg>effect=LAVA_POP</arg>
            <arg>effect=HEART</arg>
            <arg>sound=ENTITY_CAT_AMBIENT</arg>
            <arg>material=STICK</arg>
            <arg>projectile=org.bukkit.entity.Arrow</arg>
        </item>
    </item-definition>
</itemset>

```

**Note**: when you want to use [**ItemSet**](https://bitbucket.org/Tantalos/itemset/wiki/Home) you need to add both plugins to your server